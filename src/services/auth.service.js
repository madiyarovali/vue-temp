const register = async (name, surname, password, birthday, email) => {
    var headers = {
        'Content-Type': 'application/json; charset=UTF-8',
        'Accepts': 'application/json; charset=UTF-8',
    };
    var method = 'POST';
    var body = JSON.stringify({
        name, surname, password, birthday, email
    });
    var url = 'http://host-11/api/register';

    var result = await fetch(url,
        {
            headers,
            method,
            body
        }
    )
    .then(response => {
        return response.json();
    });
    console.log(result);
    return result;
}

const auth = {
    register
};

export default auth;